![Hallway Studios](http://email.hallwaystudios.com/logo-height80.gif "Hallway Studios")

# jQuery breakPoint Plugin

This jQuery plugin allows you to specify breakpoints to simplify the process of building responsive websites.
It can be used as an aid to CSS to allow media queries on all JavaScript enabled browsers or to enhance 
responsive functionality.

## Usage

For basic usage, you can call `$(window).breakPoint()` to enable the breakpoint functionality. 
When maximum dimensions are reached, a class will be added to the document body. By default, the breakpoints set 
are `1280,1024,768,480,320` although more breakpoints can be added by adding the `breakpoints` argument. E.g. 
`$(window).breakPoint({ breakpoints : [1920] })`. A class will now be added to the body element when the maximum 
width of the viewport is 1920 pixels.

In addition to classes, the breakPoint plugin also allows you to bind actions to given breakpoint events. The below 
example, demonstrates usage to display a popup notification when hitting the 1024 breakpoint.

	// bind to 1024 breakpoint entry
	$(window).bind('enterBreakpoint1024', function () {
		alert('You have just ENTERED the 1024 breakpoint');
	});
	
	// bind to 1024 breakpoint exit
	$(window).bind('exitBreakpoint1024', function () {
		alert('You have just EXITED the 1024 breakpoint');
	});
	
	// set-up breakpoint functionality (in this example,
	// only 1024 breakpoint will be active)
	$(window).breakPoint({ breakpoints : [1024] });
	
Each active breakpoint will have a class and two triggers, enter and exit. This means you can easily apply animations, 
for example when entering or exiting a breakpoint or just show and hide elements using the appropriate CSS.