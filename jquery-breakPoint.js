/**
* jQuery Breakpoint Plugin
* Advanced window breakpoint functionality aiding responsive functionality
*
* @version 0.1
* @author Chris Page
*/
(function($) {
    $.fn.breakPoint = function (settings) {

        // if breakpoints have not been specified, set a series of defaults
        var options = $.extend({
            breakpoints : [1280,1024,768,480,320],
			debug : false
        }, settings);

        // test change

        // store body in variable
        var _body = $('BODY');

        // create global used breakpoints array
        window.usedBreakpoints = new Array();

        // loop through each breakpoint
        for(var bp in options.breakpoints) {

            // store the current breakpoint in a variable
            var _breakPoint = options.breakpoints[bp];

            // create a trigger for each breakpoint check and then set them off to start
            $(window).bind('triggerBreakpoint' + _breakPoint, function (e, data) {

                // set variables to handle resizing
                var _window = $(window);
                var _windowWidth = _window.outerWidth();

                // if the breakpoint is less than the window width
                if(_windowWidth <= data.breakPoint && window.usedBreakpoints.indexOf(data.breakPoint) == -1) {

                    // trigger window enter breakpoint event and add class
                    _body.addClass('Breakpoint' + data.breakPoint);
                    _window.trigger('enterBreakpoint' + data.breakPoint);
					if(options.debug) console.log('Triggered enterBreakpoint' + data.breakPoint);

                    // add breakpoint to the array
                    window.usedBreakpoints.push(data.breakPoint);

                } else if(_windowWidth > data.breakPoint && window.usedBreakpoints.indexOf(data.breakPoint) > -1) {

                    // trigger window exit breakpoint event and remove class
                    _body.removeClass('Breakpoint' + data.breakPoint);
                    _window.trigger('exitBreakpoint' + data.breakPoint);
					if(options.debug) console.log('Triggered exitBreakpoint' + data.breakPoint);

                    // remove breakpoint from the array
                    window.usedBreakpoints.splice(window.usedBreakpoints.indexOf(data.breakPoint),1);
                }

            // trigger the breakpoint to get things going initially
            }).trigger('triggerBreakpoint' + _breakPoint, { 'breakPoint' : _breakPoint });
        }

        // trigger breakpoints on window resize
        $(window).resize(function () {
            // loop through each breakpoint and trigger
            for(var bp in options.breakpoints) {
                // trigger each breakpoint on resize
                $(window).trigger('triggerBreakpoint' + options.breakpoints[bp], { 'breakPoint' : options.breakpoints[bp] });
            }
        });

    }
})(jQuery);